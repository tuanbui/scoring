# Release

You have successfully implemented three features, Mean Squared Error (MSE), input validation and logging. This accomplishment fulfills the requirements for proceeding with a release. As a projet leader, you:
1. Merge develop into main
```bash
git pull origin develop
git checkout main
git merge develop
git push origin main
```

2. Create a tag
```bash
git tag -a v2.0 -m "Version 2.0"
```

3. Push your tags
```bash
git push origin --tags
```

4. Create a release
    * On Gitlab project page, Select Deploy > Releases and select New release.
    * Select an existing Git tag `v2.0`
    * Enter additional information about the release, including:
        * Release title: Leave blank
        * Milestones: Leave blank
        * Release notes: Add three new features: Mean Squared Error (MSE), input validation and logging
    * Select Create release.
