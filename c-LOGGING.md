# Logging

Implement logging to capture the process and any potential errors. This will help in debugging and maintenance.

## Implement logging

<details>
<summary>Solution</summary>
<p>

- Create a feature `c_logging` branch:

```bash
git checkout develop
git pull origin develop
git checkout -b c_logging
```

</p>

<p>

- `score.py`:

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
from sys import argv
import numpy as np
import logging

# Configure logging
logging.basicConfig(filename='score.log', filemode='w', 
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', 
                    level=logging.DEBUG)
    

# ========= Useful functions ==============
def read_array(filename):
    ''' Read array and convert to 2d np arrays '''
    logging.debug(f"Attempting to read file: {filename}")

    if not os.path.exists(filename):
        logging.error(f"The file {filename} does not exist.")
        raise FileNotFoundError(f"The file {filename} does not exist.")
    
    formatted_array = []

    with open(filename, 'r') as file:
        for line_num, line in enumerate(file, start=1):
            # Split the line into elements and strip whitespace
            elements = line.strip().split()

            # Check if there are exactly three elements
            if len(elements) != 3:
                logging.error(f"Error in {filename}, line {line_num}: Expected 3 elements, found {len(elements)}")
                raise ValueError(f"Error in {filename}, line {line_num}: Expected 3 elements, found {len(elements)}")

            # Check if all elements are either '0' or '1'
            if not all(elem in ['0', '1'] for elem in elements):
                logging.error(f"Error in {filename}, line {line_num}: Elements must be '0' or '1'")
                raise ValueError(f"Error in {filename}, line {line_num}: Elements must be '0' or '1'")

            # Convert elements to integers and add to the array
            formatted_array.append([int(elem) for elem in elements])

    logging.info(f"File {filename} read successfully.")
    # Convert the list to a numpy array
    return np.array(formatted_array)


def accuracy_metric(solution, prediction):
    logging.debug("Calculating accuracy metric")
    if len(solution) == 0 or len(prediction) == 0:
        logging.warning("Received empty array(s) in accuracy_metric")
        return 0
    correct_samples = np.all(solution == prediction, axis=1)
    accuracy = np.mean(correct_samples)
    logging.info(f"Accuracy metric calculated successfully: {accuracy}")
    return accuracy


def mse_metric(solution, prediction):
    '''Mean-square error.
    Works even if the target matrix has more than one column'''
    logging.debug("Calculating MSE metric")
    if len(solution) == 0 or len(prediction) == 0:
        logging.warning("Received empty array(s) in mse_metric")
        return 0
    mse = np.sum((solution - prediction)**2, axis=1)
    mse = np.mean(mse)
    logging.info(f"MSE metric calculated successfully: {mse}")
    return mse


def _HERE(*args):
    h = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(h, *args)
    

# =============================== MAIN ========================================
if __name__ == "__main__":

    #### INPUT/OUTPUT: Get input and output directory names
    try:
        logging.debug("Score execution started.")
        prediction_file = argv[1]
        solution_file = argv[2]
        # Read the solution and prediction values into numpy arrays
        solution = read_array(solution_file)
        prediction = read_array(prediction_file)
    except IndexError:
        logging.error("Incorrect usage: script requires two arguments for prediction and solution files.")
        print("Usage: script.py predict.txt solution.txt")
        sys.exit(1)
    except (FileNotFoundError, IOError) as e:
        logging.error(e)
        sys.exit(1)
    
    score_file = open(_HERE('scores.txt'), 'w')
    # # Extract the dataset name from the file name
    prediction_name = os.path.basename(prediction_file)
    
    # Check if the shapes of the arrays are compatible
    if prediction.shape != solution.shape:
        logging.error("Error: Prediction and solution arrays have different shapes.")
        sys.exit(1)
    
    # Compute the score prescribed by the metric file 
    accuracy_score = accuracy_metric(solution, prediction)
    mse_score = mse_metric(solution, prediction)
    print(
        "======= (" + prediction_name + "): score(accuracy_metric)=%0.2f =======" % accuracy_score)
    print(
        "======= (" + prediction_name + "): score(mse_metric)=%0.2f =======" % mse_score)
    # Write score corresponding to selected task and metric to the output file
    score_file.write("accuracy_metric: %0.2f\n" % accuracy_score)
    score_file.write("mse_metric: %0.2f\n" % mse_score)
    score_file.close()
    logging.info("Score completed successfully")
```

</p>

<p>

- See the logs:

```bash
./score.py predict.txt solution.txt
cat score.log
```

</p>
</details>

## Create a merge request

## Approve a merge request

## Merge `c_logging` into `develop`